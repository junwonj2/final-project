package com.example.junwon.finalproject;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import me.grantland.widget.AutofitTextView;

/**
 * Created by Jun Won on 2017-12-08.
 */

public class ActorListFragment extends Fragment {

    View view;
    ListView mListView;
    int movie_id;
    String character;
    String name;
    String profile_path;
    int actor_id;
    String credit_id;
    int gender;
    Bitmap bit;
    List<ActorInfo> ActorList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.collection_fragment, container, false);
        mListView = (ListView) view.findViewById(R.id.collection_listview);
        setListViewScrollable();
        getMovieIdFromBundle();
        getActorListData();
        mListView.setFocusable(false);
        return view;
    }

    /**
     * Make listview scrollable when in the scrollview
     */
    private void setListViewScrollable() {
        mListView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
    }

    /**
     * Get data from bundle in the previous fragment replacing
     */

    private void getMovieIdFromBundle() {
        Bundle bundle = getArguments();
        movie_id = bundle.getInt("movie_id");
    }

    /**
     * get data of actor list and show it to the frame
     */
    public void getActorListData() {
        new AsyncTask() {

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    JSONObject JsonObject = new JSONObject(HttpRequest.get("http://api.themoviedb.org/3/movie/" + movie_id + "/casts",
                            true, "api_key", MainActivity.getApi_key()).body());
                    JSONArray JsonArray = JsonObject.getJSONArray("cast");
                    for (int i = 0; i < JsonArray.length(); i++) {
                        JSONObject cast_JsonObject = JsonArray.getJSONObject(i);
                        character = cast_JsonObject.getString("character");
                        credit_id = cast_JsonObject.getString("credit_id");
                        gender = cast_JsonObject.getInt("gender");
                        actor_id = cast_JsonObject.getInt("id");
                        name = cast_JsonObject.getString("name");
                        profile_path = cast_JsonObject.getString("profile_path");
                        bit = getBitmap("http://image.tmdb.org/t/p/w185" + profile_path);
                        ActorInfo actorInfo = new ActorInfo(character, credit_id, gender, actor_id, name, bit);
                        ActorList.add(actorInfo);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                CustomAdapter customAdapter = new CustomAdapter();
                mListView.setAdapter(customAdapter);
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ActorInfo actorInfo = ActorList.get(position);
                        Bundle bundle = new Bundle();
                        bundle.putInt("person_id", actorInfo.actor_id);
                        Fragment fragment = new DetailedActorFragment();
                        fragment.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).addToBackStack(null).commit();
                    }
                });
            }
        }.execute();
    }

    /**
     * get bitmap image from the link indicated
     *
     * @param actual_link link to get the image
     * @return bitmap image
     * @throws IOException exception for IO
     */

    private Bitmap getBitmap(String actual_link) throws IOException {
        URL urlConnection = new URL(actual_link);
        HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
    }

    /**
     * private class about actor info
     */


    private class ActorInfo {

        String character;
        String name;
        Bitmap bitmap;
        int actor_id;
        String credit_id;
        int gender;

        /**
         * Constructor of Actor Info
         *
         * @param character character string
         * @param credit_id credit id string
         * @param gender    gender as integer
         * @param actor_id  int as actor id
         * @param name      name as string
         * @param bitmap    bitmap as Bitmap file
         */

        public ActorInfo(String character, String credit_id, int gender, int actor_id, String name, Bitmap bitmap) {
            this.character = character;
            this.name = name;
            this.actor_id = actor_id;
            this.credit_id = credit_id;
            this.gender = gender;
            this.bitmap = bitmap;
        }
    }

    /**
     * custom adapter about listview
     */

    private class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return ActorList.size();
        }

        @Override
        public Object getItem(int position) {
            return ActorList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getActivity().getLayoutInflater().inflate(R.layout.actor_list_fragment, null);
            AutofitTextView actor_character = (AutofitTextView) convertView.findViewById(R.id.actor_character);
            AutofitTextView actor_name = (AutofitTextView) convertView.findViewById(R.id.actor_name);
            AutofitTextView actor_gender = (AutofitTextView) convertView.findViewById(R.id.actor_gender);

            ImageView actor_image = (ImageView) convertView.findViewById(R.id.actor_image);

            ActorInfo actorInfo = ActorList.get(position);
            actor_character.setText("Character: " + actorInfo.character);
            actor_name.setText("Name: " + actorInfo.name);
            int gender = actorInfo.gender;
            if (gender == 1) {
                actor_gender.setText("Gender: F");
            } else {
                actor_gender.setText("Gender: M");
            }
            actor_image.setImageBitmap(actorInfo.bitmap);
            return convertView;
        }
    }
}
