package com.example.junwon.finalproject;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Jun Won on 2017-12-04.
 */

public class RecentSearchedFragment extends Fragment{

    View view;

    /**
     * Rated Movie fragment with putting arguments combining MovieListFragment
     *
     * @return returning a view of its fragment
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.collection_container_fragment,container,false);
        getActivity().setTitle("Recent Searched Movies");
        MainActivity.deleteMenu = true;
        Fragment fragment = new MovieListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("flag","recent");
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.collection_container,fragment).commit();
        return view;
    }
}
