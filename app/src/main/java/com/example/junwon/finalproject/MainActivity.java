package com.example.junwon.finalproject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private ActionBarDrawerToggle mToggle;
    private NavigationView navigationView;
    private Toolbar mToolbar;
    private final static String api_key = "7a3ab68737e6c2d44b1c27c3d1b90f0a";
    private final static String session_id = "e61370e64733ae8adad24c5772016227b799667c";
    private static Bitmap bitmap = null;
    public Map<Integer, String> genre_map = new HashMap<>();
    CircleImageView header_image;
    TextView header_name;
    TextView header_username;
    static boolean deleteMenu = false;
    private static final String TAG = "MainActivity";

    /**
     * function that gets session id
     *
     * @return session id
     */
    public static String getSession_id() {
        return session_id;
    }

    /**
     * function that gets api key
     *
     * @return api key
     */
    public static String getApi_key() {
        return api_key;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigationView = (NavigationView) findViewById(R.id.navigationView);
        mToolbar = (Toolbar) findViewById(R.id.nav_action);
        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        mToggle = new ActionBarDrawerToggle(this, drawer, R.string.open, R.string.close);
        setSupportActionBar(mToolbar);
        drawer.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setGenreMap();

        setHeader();
        getFragmentManager().beginTransaction().replace(R.id.main_frame, new MovieProfileFragment()).commit();
        navigationView.setNavigationItemSelectedListener(this);
    }

    /**
     * setting up all variables
     */

    private void setHeader() {
        View mView = getLayoutInflater().inflate(R.layout.header_container, null);
        header_image = (CircleImageView) mView.findViewById(R.id.header_image);
        header_name = (TextView) mView.findViewById(R.id.header_name);
        header_username = (TextView) mView.findViewById(R.id.header_username);
        new ProfileData();
        navigationView.addHeaderView(mView);
    }

    /**
     * change integer genre code to the string type
     */

    private void setGenreMap() {
        AsyncTask asynctask = new AsyncTask() {

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    JSONObject jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/genre/movie/list",
                            true, "api_key", api_key).body());
                    JSONArray genre_array = jsonObject.getJSONArray("genres");
                    for (int i = 0; i < genre_array.length(); i++) {
                        JSONObject genre_object = genre_array.getJSONObject(i);
                        int id = genre_object.getInt("id");
                        String name = genre_object.getString("name");
                        genre_map.put(id, name);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * uncheck all items in the menu
     */

    public void unCheckItems() {
        for (int i = 0; i < navigationView.getMenu().size(); i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_profile:
                getFragmentManager().beginTransaction().replace(R.id.main_frame, new MovieProfileFragment()).commit();
                unCheckItems();
                item.setChecked(true);
                break;
            case R.id.nav_watchlist:
                getFragmentManager().beginTransaction().replace(R.id.main_frame, new MyWatchlistFragment()).commit();
                unCheckItems();
                item.setChecked(true);
                break;
            case R.id.nav_upcoming:
                getFragmentManager().beginTransaction().replace(R.id.main_frame, new UpcomingCollectionFragment()).commit();
                unCheckItems();
                item.setChecked(true);
                break;
            case R.id.nav_rated:
                getFragmentManager().beginTransaction().replace(R.id.main_frame, new RatedMovieFragment()).commit();
                unCheckItems();
                item.setChecked(true);
                break;
            case R.id.nav_favorite:
                getFragmentManager().beginTransaction().replace(R.id.main_frame, new FavoriteFragment()).commit();
                unCheckItems();
                item.setChecked(true);
                break;
            case R.id.nav_search:
                getFragmentManager().beginTransaction().replace(R.id.main_frame, new SearchMovieFragment()).commit();
                unCheckItems();
                item.setChecked(true);
                break;
            case R.id.nav_recent_searched:
                getFragmentManager().beginTransaction().replace(R.id.main_frame, new RecentSearchedFragment()).commit();
                unCheckItems();
                item.setChecked(true);
                break;
        }
        return true;
    }

    /**
     * get bitmap image from the link indicated
     *
     * @param actual_link link to get the image
     * @return bitmap image
     * @throws IOException exception for IO
     */

    private Bitmap getBitmap(String actual_link) throws IOException {
        URL urlConnection = new URL(actual_link);
        HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
    }

    /**
     * private class to hold the data
     */

    private class ProfileData {
        String hash;
        String name;
        String username;
        Bitmap profile_image;

        /**
         * constructor of its class
         */
        public ProfileData() {
            AsyncTask asyncTask = new AsyncTask() {
                @Override
                protected void onPostExecute(Object o) {
                    header_image.setImageBitmap(profile_image);
                    header_name.setText(name);
                    header_username.setText(username);
                }

                @Override
                protected Object doInBackground(Object[] params) {

                    try {
                        JSONObject jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/account",
                                true, "api_key", MainActivity.getApi_key(), "session_id", MainActivity.getSession_id()).body());
                        JSONObject avatar_object = jsonObject.getJSONObject("avatar");
                        JSONObject gravatar_object = avatar_object.getJSONObject("gravatar");
                        hash = gravatar_object.getString("hash");
                        name = jsonObject.getString("name");
                        username = jsonObject.getString("username");
                        String actual_link = "https://www.gravatar.com/avatar/" + hash;
                        profile_image = getBitmap(actual_link);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }.execute();
        }
    }
}
