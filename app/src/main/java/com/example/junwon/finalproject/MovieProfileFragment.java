package com.example.junwon.finalproject;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static android.R.attr.format;

/**
 * Created by Jun Won on 2017-11-18.
 */

public class MovieProfileFragment extends Fragment {

    View view;
    private ImageView profileImage;
    private TextView profileName;
    private TextView rated;
    private TextView average;
    private TextView favorite;
    private ImageView rated_movie1;
    private ImageView rated_movie2;
    private ImageView rated_movie3;
    private ImageView favorite_movie1;
    private ImageView favorite_movie2;
    private ImageView favorite_movie3;
    private ArrayList<Bitmap> rated_movie = new ArrayList<>();
    private ArrayList<Bitmap> favorite_movie = new ArrayList<>();
    private int rated_num = 0;
    private int sum_rated = 0;
    private int favorite_num = 0;
    private ArrayList<Integer> favorite_movie_id = new ArrayList<>();
    private ArrayList<Integer> rated_movie_id = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.profile_fragment, container, false);
        getActivity().setTitle("Profile");
        setVariables();
        MainActivity.deleteMenu = true;
        try {
            setRatedMovies();
            setFavoriteMovies();
            new ProfileData();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return view;
    }

    /**
     * Setting up the rated Movies in the profile page
     *
     * @throws JSONException throwing JSONException
     */

    private void setRatedMovies() throws JSONException {
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected void onPostExecute(Object o) {
                try {
                    rated_movie1.setImageBitmap(rated_movie.get(0));
                    rated_movie2.setImageBitmap(rated_movie.get(1));
                    rated_movie3.setImageBitmap(rated_movie.get(2));
                    rated.setText(String.valueOf(rated_num));
                    DecimalFormat df = new DecimalFormat("#.##");
                    float average_num = Float.valueOf(df.format((float) sum_rated / rated_num));
                    average.setText(String.valueOf(average_num));
                    setRatedMovieItemClick();
                } catch (Exception e) {
                }
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    JSONObject jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/account/{account_id}/rated/movies",
                            true, "api_key", MainActivity.getApi_key(), "session_id", MainActivity.getSession_id(), "sort_by", "created_at.desc").body());
                    rated_num = jsonObject.getInt("total_results");
                    int total_pages = jsonObject.getInt("total_pages");
                    for (int j = 1; j <= total_pages; j++) {
                        jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/account/{account_id}/rated/movies",
                                true, "api_key", MainActivity.getApi_key(), "session_id", MainActivity.getSession_id(), "sort_by", "created_at.desc",
                                "page", j).body());
                        JSONArray jsonArray_result = jsonObject.getJSONArray("results");
                        for (int i = 0; i < jsonArray_result.length(); i++) {
                            JSONObject json_result = jsonArray_result.getJSONObject(i);
                            String link = json_result.getString("poster_path");
                            String actual_link = "http://image.tmdb.org/t/p/w185" + link;
                            rated_movie_id.add(json_result.getInt("id"));
                            Bitmap myBitmap = getBitmap(actual_link);
                            rated_movie.add(myBitmap);
                            sum_rated += json_result.getInt("rating");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    /**
     * The function that sets up clicking item of the rated image
     */

    private void setRatedMovieItemClick() {
        rated_movie1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("movie_id", rated_movie_id.get(0));
                Fragment fragment = new DetailedMovieFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).addToBackStack(null).commit();
            }
        });
        rated_movie2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("movie_id", rated_movie_id.get(1));
                Fragment fragment = new DetailedMovieFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).addToBackStack(null).commit();
            }
        });
        rated_movie3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("movie_id", rated_movie_id.get(2));
                Fragment fragment = new DetailedMovieFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).addToBackStack(null).commit();
            }
        });
    }

    /**
     * The function that sets up clicking item of the favorite image
     */

    private void setFavoriteMovieItemClick() {
        favorite_movie1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("movie_id", favorite_movie_id.get(0));
                Fragment fragment = new DetailedMovieFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).addToBackStack(null).commit();
            }
        });
        favorite_movie2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("movie_id", favorite_movie_id.get(1));
                Fragment fragment = new DetailedMovieFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).addToBackStack(null).commit();
            }
        });
        favorite_movie3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("movie_id", favorite_movie_id.get(2));
                Fragment fragment = new DetailedMovieFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).addToBackStack(null).commit();
            }
        });
    }

    /**
     * get bitmap image from the link indicated
     *
     * @param actual_link link to get the image
     * @return bitmap image
     * @throws IOException exception for IO
     */

    private Bitmap getBitmap(String actual_link) throws IOException {
        URL urlConnection = new URL(actual_link);
        HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
    }

    /**
     * The function that setting up basic data things of favorite movies
     */

    private void setFavoriteMovies() throws JSONException, ExecutionException, InterruptedException, IOException {
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected void onPostExecute(Object o) {
                try {
                    favorite_movie1.setImageBitmap(favorite_movie.get(0));
                    favorite_movie2.setImageBitmap(favorite_movie.get(1));
                    favorite_movie3.setImageBitmap(favorite_movie.get(2));
                    favorite.setText(String.valueOf(favorite_num));
                    setFavoriteMovieItemClick();
                } catch (Exception e) {
                }
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    JSONObject jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/account/{account_id}/favorite/movies",
                            true, "api_key", MainActivity.getApi_key(), "session_id", MainActivity.getSession_id(), "sort_by", "created_at.desc").body());
                    favorite_num = jsonObject.getInt("total_results");
                    JSONArray jsonArray_result = jsonObject.getJSONArray("results");
                    for (int i = 0; i < jsonArray_result.length(); i++) {
                        JSONObject json_result = jsonArray_result.getJSONObject(i);
                        String link = json_result.getString("poster_path");
                        String actual_link = "http://image.tmdb.org/t/p/w185" + link;
                        favorite_movie_id.add(json_result.getInt("id"));
                        Bitmap bit = getBitmap(actual_link);
                        favorite_movie.add(bit);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    /**
     * Setting up variables in this class
     */

    private void setVariables() {
        profileImage = (ImageView) view.findViewById(R.id.profileImage);
        rated = (TextView) view.findViewById(R.id.rated_num);
        average = (TextView) view.findViewById(R.id.average_num);
        favorite = (TextView) view.findViewById(R.id.favorite_num);
        rated_movie1 = (ImageView) view.findViewById(R.id.rated_movie_image1);
        rated_movie2 = (ImageView) view.findViewById(R.id.rated_movie_image2);
        rated_movie3 = (ImageView) view.findViewById(R.id.rated_movie_image3);
        favorite_movie1 = (ImageView) view.findViewById(R.id.recommended_movies_image1);
        favorite_movie2 = (ImageView) view.findViewById(R.id.recommended_movies_image2);
        favorite_movie3 = (ImageView) view.findViewById(R.id.recommended_movies_image3);
        profileName = (TextView) view.findViewById(R.id.profileName);
    }

    /**
     * private class to hold the data
     */

    private class ProfileData {
        String hash;
        String name;
        String username;
        Bitmap profile_image;

        public ProfileData() {
            AsyncTask asyncTask = new AsyncTask() {
                @Override
                protected void onPostExecute(Object o) {
                    profileImage.setImageBitmap(profile_image);
                    profileName.setText("Name: " + name + "\nUsername: " + username);
                }

                @Override
                protected Object doInBackground(Object[] params) {

                    try {
                        JSONObject jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/account",
                                true, "api_key", MainActivity.getApi_key(), "session_id", MainActivity.getSession_id()).body());
                        JSONObject avatar_object = jsonObject.getJSONObject("avatar");
                        JSONObject gravatar_object = avatar_object.getJSONObject("gravatar");
                        hash = gravatar_object.getString("hash");
                        name = jsonObject.getString("name");
                        username = jsonObject.getString("username");
                        String actual_link = "https://www.gravatar.com/avatar/" + hash;
                        profile_image = getBitmap(actual_link);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }.execute();
        }
    }
}
