package com.example.junwon.finalproject;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jun Won on 2017-12-09.
 */

public class CacheBitmapImage {

    static Map<Integer, Bitmap> cacheImage = new HashMap();

    /**
     * function that put the image bitmap into the class
     *
     * @param movie_id movie id to get from hashmap
     * @param bitmap   image to put in hashmap
     */

    public static void putImage(int movie_id, Bitmap bitmap) {
        cacheImage.put(movie_id, bitmap);
    }

    /**
     * boolean function that checks if it is in cache map
     *
     * @param movie_id movie id to check if it is in hashmap
     * @return true if it is in hash, false if it is not
     */
    static boolean isInCache(int movie_id) {
        if (cacheImage.get(movie_id) == null)
            return false;
        return true;
    }

    /**
     * get bimap from hashmap
     *
     * @param movie_id movie id to get it from hashmap
     * @return bitmap returning of its key
     */

    static Bitmap getImage(int movie_id) {
        return cacheImage.get(movie_id);
    }

}
