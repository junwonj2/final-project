package com.example.junwon.finalproject;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.solver.Cache;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jun Won on 2017-12-10.
 */

public class DetailedActorFragment extends Fragment {

    View view;
    int person_id;
    ImageView actorPoster;
    TextView actorGender;
    TextView actorBirthday;
    TextView actorPlaceOfBirth;
    TextView actorDetailName;
    TextView actorDetailBiography;
    TextView actorDetailHomepage;
    ActorInfo actorInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.custom_actor_fragment, container, false);
        getPersonId();
        saveAllVariables();
        getDataAndSetToText();
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * getting person id from bundle
     */
    private void getPersonId() {
        Bundle bundle = getArguments();
        person_id = bundle.getInt("person_id");
    }

    /**
     * get data from json file and set it to text in the frame
     */

    private void getDataAndSetToText() {
        new AsyncTask() {

            @Override
            protected void onPostExecute(Object o) {
                actorPoster.setImageBitmap(actorInfo.bitmap);
                if (actorInfo.gender == 1) {
                    actorGender.setText("Gender: F");
                } else {
                    actorGender.setText("Gender: M");
                }
                actorBirthday.setText("Birthday: " + actorInfo.birthday);
                actorPlaceOfBirth.setText("Place of Birth: " + actorInfo.place_of_birth);
                actorDetailName.setText(actorInfo.name);
                actorDetailBiography.setText(actorInfo.biography);
                actorDetailHomepage.setText("Homepage: " + actorInfo.homepage);
                Fragment fragment = new DetailedActorMovieListFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("person_id", person_id);
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.actor_detail_movielist, fragment).commit();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    JSONObject JsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/person/" + person_id,
                            true, "api_key", MainActivity.getApi_key()).body());
                    String birthday = JsonObject.getString("birthday");
                    int id = JsonObject.getInt("id");
                    int gender = JsonObject.getInt("gender");
                    String name = JsonObject.getString("name");
                    String biography = JsonObject.getString("biography");
                    String place_of_birth = JsonObject.getString("place_of_birth");
                    String profile_path = JsonObject.getString("profile_path");
                    String homepage = JsonObject.getString("homepage");
                    Bitmap bitmap = null;
                    if (!CacheBitmapImage.isInCache(id)) {
                        bitmap = getBitmap("http://image.tmdb.org/t/p/w185" + profile_path);
                        CacheBitmapImage.putImage(id, bitmap);
                    } else {
                        bitmap = CacheBitmapImage.getImage(id);
                    }
                    actorInfo = new ActorInfo(birthday, id, gender, name, biography, place_of_birth, profile_path, homepage, bitmap);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    /**
     * get bitmap image from the link indicated
     *
     * @param actual_link link to get the image
     * @return bitmap image
     * @throws IOException exception for IO
     */

    private Bitmap getBitmap(String actual_link) throws IOException {
        URL urlConnection = new URL(actual_link);
        HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
    }

    /**
     * save all variables that requires in this frame
     */

    private void saveAllVariables() {
        actorPoster = (ImageView) view.findViewById(R.id.detailed_actor_poster);
        actorGender = (TextView) view.findViewById(R.id.actor_detail_gender);
        actorBirthday = (TextView) view.findViewById(R.id.actor_detail_birthday);
        actorPlaceOfBirth = (TextView) view.findViewById(R.id.actor_detail_placeofbirth);
        actorDetailName = (TextView) view.findViewById(R.id.actor_detail_name);
        actorDetailBiography = (TextView) view.findViewById(R.id.actor_detail_biography);
        actorDetailHomepage = (TextView) view.findViewById(R.id.actor_detail_homepage);
    }

    /**
     * class that holds info of actors
     */
    private class ActorInfo {
        String birthday;
        int id;
        int gender;
        String name;
        String biography;
        String place_of_birth;
        String profile_path;
        String homepage;
        Bitmap bitmap;

        public ActorInfo(String birthday, int id, int gender,
                         String name, String biography, String place_of_birth, String profile_path, String homepage, Bitmap bitmap) {
            this.birthday = birthday;
            this.id = id;
            this.gender = gender;
            this.name = name;
            this.biography = biography;
            this.place_of_birth = place_of_birth;
            this.profile_path = profile_path;
            this.homepage = homepage;
            this.bitmap = bitmap;
        }
    }
}
