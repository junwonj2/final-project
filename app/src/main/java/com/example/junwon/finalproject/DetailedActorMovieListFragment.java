package com.example.junwon.finalproject;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jun Won on 2017-12-10.
 */

public class DetailedActorMovieListFragment extends Fragment {

    View view;
    int person_id;
    List<ActorMovieListInfo> actorMovieListInfoList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.collection_fragment, container, false);
        getPersonId();
        getDataAndSetText();
        return view;
    }

    /**
     * get person id from bundle
     */
    private void getPersonId() {
        Bundle bundle = getArguments();
        person_id = bundle.getInt("person_id");
    }

    /**
     * let listview scrollable in the scrollview
     *
     * @param listView the listview to contribute to this function
     */
    private void setListViewScrollable(ListView listView) {
        listView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
    }

    /**
     * get data from json file and set it as a text or poster it
     */

    private void getDataAndSetText() {
        new AsyncTask() {

            @Override
            protected void onPostExecute(Object o) {
                ListView listView = (ListView) view.findViewById(R.id.collection_listview);
                CustomAdapter customAdapter = new CustomAdapter();
                listView.setAdapter(customAdapter);
                listView.setFocusable(false);
                setListViewScrollable(listView);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        int movie_id = actorMovieListInfoList.get(position).id;
                        Bundle bundle = new Bundle();
                        bundle.putInt("movie_id", movie_id);
                        Fragment fragment = new DetailedMovieFragment();
                        fragment.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).addToBackStack(null).commit();
                    }
                });
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    JSONObject jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/person/" + person_id + "/movie_credits",
                            true, "api_key", MainActivity.getApi_key()).body());
                    JSONArray castArray = jsonObject.getJSONArray("cast");
                    for (int i = 0; i < castArray.length(); i++) {
                        JSONObject JsonObject = castArray.getJSONObject(i);
                        String character = JsonObject.getString("character");
                        String poster_path = JsonObject.getString("poster_path");
                        int id = JsonObject.getInt("id");
                        Bitmap bitmap = null;
                        if (!CacheBitmapImage.isInCache(id)) {
                            bitmap = getBitmap("http://image.tmdb.org/t/p/w185" + poster_path);
                            CacheBitmapImage.putImage(id, bitmap);
                        } else {
                            bitmap = CacheBitmapImage.getImage(id);
                        }
                        String title = JsonObject.getString("title");
                        String release_date = JsonObject.getString("release_date");
                        int vote_average = JsonObject.getInt("vote_average");
                        ActorMovieListInfo actorMovieListInfo = new ActorMovieListInfo(character, bitmap, id, title, release_date, vote_average);
                        actorMovieListInfoList.add(actorMovieListInfo);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    /**
     * get bitmap image from the link indicated
     *
     * @param actual_link link to get the image
     * @return bitmap image
     * @throws IOException exception for IO
     */

    private Bitmap getBitmap(String actual_link) throws IOException {
        URL urlConnection = new URL(actual_link);
        HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
    }

    /**
     * class that holds info of its actor
     */

    private class ActorMovieListInfo {

        String character;
        Bitmap bitmap;
        int id;
        String title;
        String release_date;
        int vote_average;

        /**
         * constructor of its class setting all of its variables
         */
        public ActorMovieListInfo(String character, Bitmap bitmap, int id, String title, String release_date, int vote_average) {
            this.character = character;
            this.bitmap = bitmap;
            this.id = id;
            this.title = title;
            this.release_date = release_date;
            this.vote_average = vote_average;
        }
    }

    /**
     * custom adapter to apply as a listview
     */

    private class CustomAdapter extends BaseAdapter {

        ImageView poster;
        TextView character;
        TextView title;
        TextView vote_average;
        TextView release_date;

        @Override
        public int getCount() {
            return actorMovieListInfoList.size();
        }

        @Override
        public Object getItem(int position) {
            return actorMovieListInfoList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getActivity().getLayoutInflater().inflate(R.layout.custom_actor_movielist_fragment, null);
            setAllVariables(convertView);
            setDataToVariables(position);
            return convertView;
        }

        /**
         * set the text or image as a given data
         * @param position  the position to get a hatch
         */

        private void setDataToVariables(int position) {
            ActorMovieListInfo actorMovieListInfo = actorMovieListInfoList.get(position);
            poster.setImageBitmap(actorMovieListInfo.bitmap);
            character.setText(actorMovieListInfo.character);
            title.setText(actorMovieListInfo.title);
            vote_average.setText(String.valueOf(actorMovieListInfo.vote_average));
            release_date.setText(actorMovieListInfo.release_date);
        }

        /**
         *
         * @param convertView
         */

        private void setAllVariables(View convertView) {
            poster = (ImageView) convertView.findViewById(R.id.custom_actor_movielist_poster);
            character = (TextView) convertView.findViewById(R.id.custom_actor_movielist_character);
            title = (TextView) convertView.findViewById(R.id.custom_actor_movielist_title);
            vote_average = (TextView) convertView.findViewById(R.id.custom_actor_movielist_vote_average);
            release_date = (TextView) convertView.findViewById(R.id.custom_actor_movielist_release_date);
        }
    }
}
