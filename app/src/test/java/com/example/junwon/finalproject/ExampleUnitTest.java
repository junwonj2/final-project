package com.example.junwon.finalproject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    private static final String TAG = "ExampleUnitTest";

    public String api_key = "7a3ab68737e6c2d44b1c27c3d1b90f0a";
    public String session_id = "e61370e64733ae8adad24c5772016227b799667c";

    @Test
    public void responseTest() {
        int response = HttpRequest.get("http://google.com").code();
        System.out.println(response);
    }

    @Test
    public void responseBodyTest() {
        String response = HttpRequest.get("http://google.com").body();
        System.out.println("Response was: " + response);
    }

    @Test
    public void recieveTest() {
        HttpRequest.get("http://google.com").receive(System.out);
    }

    @Test
    public void addingParameterTest() {
        HttpRequest request = HttpRequest.get("http://google.com", true, 'q', "baseball gloves", "size", 100);
        System.out.println(request.toString());
    }

    @Test
    public void getAccountInfoTest() {
        HttpRequest request = HttpRequest.get("https://api.themoviedb.org/3/account", true, "api_key", api_key, "session_id", session_id);
        System.out.println(request.code());
        System.out.println(request.body());
    }

    @Test
    public void debuggingTest() throws JSONException, UnirestException {
        Map<String, Float> map = new HashMap<>();
        map.put("value", (float) 8.5);
        String body = HttpRequest.post("https://api.themoviedb.org/3/movie/141052/rating", true, "api_key", api_key, "session_id", session_id).form(map).body();
        System.out.println(body);
    }

    @Test
    public void seconddebuggingTest() throws JSONException, UnirestException {
        Map<String, Object> map = new HashMap<>();
        map.put("media_type", "movie");
        map.put("media_id", 440021);
        map.put("favorite", true);
        String body = HttpRequest.post("https://api.themoviedb.org/3/account/{account_id}/favorite", true, "api_key",
                MainActivity.getApi_key(), "session_id", MainActivity.getSession_id()).form(map).body();
        System.out.println(body);
    }

    @Test
    public void hashMapTest() {
        Map map = new HashMap();
        map.put("a", new Long(1));
        map.put("b", 2);
        map.put("c", 3);
        map.put("d", 4);
        map.put("e", 5);
        if (map.get("a") instanceof Long)
            System.out.println("hashMapTest: " + map.get("a"));
        if (map.get("b") instanceof Long)

            System.out.println("hashMapTest: " + map.get("b"));
        if (map.get("c") instanceof Long)

            System.out.println("hashMapTest: " + map.get("c"));
        if (map.get("d") instanceof Integer)

            System.out.println("hashMapTest: " + map.get("d"));
        if (map.get("e") instanceof Integer)

            System.out.println("hashMapTest: " + map.get("e"));
    }

    @Test
    public void doRecentWork() {
        Map map = new HashMap();
        map.put("a", 1);

        if (map.get("a") instanceof Integer) {
            System.out.println("doRecentWork: " + "Integer" + map.get("a"));
        }

        if (map.get("a") instanceof Long) {
            System.out.println("doRecentWork: " + "Long" + map.get("a"));
        }
    }

    @Test
    public void workOnData() {
        Map<String, Integer> map = new HashMap();
        map.put("a", 1);

        if (map.get("a") instanceof Integer) {
            System.out.println("doRecentWork: " + "Integer" + map.get("a"));
        }

        Map map2 = new HashMap();

        map2.put("a", new Integer(1));
        map2.put("b", 2);

        if (map2.get("a") instanceof Integer) {
            System.out.println("doRecentWork: " + "Integer" + map2.get("a"));
        }

        if (map2.get("b") instanceof Long) {
            System.out.println("doRecentWork: " + "Long" + map2.get("b"));
        }

        if (map2.get("b") instanceof Integer) {
            System.out.println("doRecentWork: " + "Integer" + map2.get("b"));
        }
    }

    @Test
    public void testActorDataInfo() {
        String body = HttpRequest.get("http://api.themoviedb.org/3/movie/49521/casts", true, "api_key", api_key).body();
        System.out.println(body);
    }

    @Test
    public void testCharacterDataInfo() {
        String body = HttpRequest.get("https://api.themoviedb.org/3/person/73968/movie_credits", true, "api_key", api_key).body();
        System.out.println(body);
    }

    @Test
    public void testActorDetailInfo() {
        String body = HttpRequest.get("https://api.themoviedb.org/3/person/73968", true, "api_key", api_key).body();
        System.out.println(body);
    }
}